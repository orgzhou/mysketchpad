package com.example.mysketchpad;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseQuickAdapter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Test2Activity extends AppCompatActivity {

    RecyclerView recyclerView;
    private List<TestInfo> ImgList = new ArrayList<>();
    private TestInfo testInfo;
    private String filePath,filePath1;
    private Test2Adapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test2);
        recyclerView = findViewById(R.id.rv);
        //布局管理器
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this,2);
        recyclerView.setLayoutManager(gridLayoutManager);

        filePath = Environment.getExternalStorageDirectory().toString()+File.separator+"ZJLHAHA";
        filePath1 = Environment.getExternalStorageDirectory().toString()+"/"+"ZJLHAHA";
        Log.e("zjl",filePath);
        Log.e("zjl",filePath1);
        // 得到该路径文件夹下所有的文件
        File fileALL = new File(filePath);
        Log.e("zjl",fileALL+"");
        File[] files = fileALL.listFiles();
        for(int i = 0; i < files.length; i++){
            File file = files[i];
//            ImgList.add(file.getPath());
//            file.getName();
            testInfo = new TestInfo(file.getPath(),file.getName());
            ImgList.add(testInfo);
        }
        Log.e("zjll",ImgList.toString());
        adapter = new Test2Adapter(this,R.layout.item_broken,ImgList);
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
//                String path = ImgList.get(position);
                TestInfo testInfo = (TestInfo) adapter.getData().get(position);

                Toast.makeText(Test2Activity.this,""+testInfo.getImgPath(),Toast.LENGTH_SHORT).show();
//                //bundle传递视频url
//                Bundle bundle = new Bundle();
//                //把视频url给传递过去
//                bundle.putString("pp",testInfo.getImgPath());
//                //跳转
//                startActivity(new Intent(Test2Activity.this,Main2Activity.class).putExtras(bundle));
            }
        });
    }
}
