package com.example.mysketchpad;

import android.app.Application;
import android.content.Context;

import com.blankj.utilcode.util.Utils;
import com.example.mysketchpad.white.bean.SuperModel;

/**
 * Created by zhou on 2020/2/26 13:35.
 */
public class MyApp extends Application {

    protected static android.content.Context Context;

    private static MyApp myApplication;

    private static String ShareName = "ZJL";

    @Override
    public void onCreate() {
        super.onCreate();
        Context = getApplicationContext();
        myApplication = this;

//        initOkGo();
        Utils.init(myApplication);

        SuperModel.initialize(this);
    }

    public static synchronized MyApp getInstance(){
        return myApplication;
    }
}