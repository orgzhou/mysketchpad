package com.example.mysketchpad;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.Iterator;

import androidx.annotation.Nullable;

/**
 * Created by zhou on 2020/2/11 19:07.
 */
public class DrawingView extends View {

    private int mBackgroundColor = Color.WHITE;
    private Paint mBackgroundPaint;
    private Bitmap mCanvasBitmap;
    private Canvas mDrawCanvas;
    private Paint mDrawPaint;
    private Path mDrawPath;
    private int mPaintColor = Color.BLACK;
    private ArrayList<Paint> mPaints = new ArrayList();
    private ArrayList<Path> mPaths = new ArrayList();
    private int mStrokeWidth = 10;
    private ArrayList<Paint> mUndonePaints = new ArrayList();
    private ArrayList<Path> mUndonePaths = new ArrayList();


    public DrawingView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        this.mDrawPath = new Path();
        this.mBackgroundPaint = new Paint();
        initPaint();
    }

    //画笔
    private void initPaint() {
        mDrawPaint = new Paint();
        mDrawPaint.setColor(this.mPaintColor);
        mDrawPaint.setAntiAlias(true);
        mDrawPaint.setStrokeWidth((float) this.mStrokeWidth);
        mDrawPaint.setStyle(Paint.Style.STROKE);
        mDrawPaint.setStrokeJoin(Paint.Join.ROUND);
        mDrawPaint.setStrokeCap(Paint.Cap.ROUND);
        mDrawPaint.setAntiAlias(true);
        mDrawPaint.setDither(true);
    }

    private void drawBackground(Canvas canvas) {
        this.mBackgroundPaint.setColor(this.mBackgroundColor);
        this.mBackgroundPaint.setStyle(Paint.Style.FILL);
        canvas.drawRect(0.0f, 0.0f, (float) getWidth(), (float) getHeight(), this.mBackgroundPaint);
    }

    private void drawPaths(Canvas canvas) {
        int i = 0;
        Iterator it = this.mPaths.iterator();
        while (it.hasNext()) {
            canvas.drawPath((Path) it.next(), (Paint) this.mPaints.get(i));
            i++;
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        drawBackground(canvas);
        drawPaths(canvas);
        canvas.drawPath(mDrawPath,mDrawPaint);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mCanvasBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        mDrawCanvas = new Canvas(mCanvasBitmap);

    }

    public boolean onTouchEvent(MotionEvent event) {
        float touchX = event.getX();
        float touchY = event.getY();
        switch (event.getAction()) {
            case 0:
                this.mDrawPath.moveTo(touchX, touchY);
                break;
            case 1:
                this.mDrawPath.lineTo(touchX, touchY);
                this.mPaths.add(this.mDrawPath);
                this.mPaints.add(this.mDrawPaint);
                this.mDrawPath = new Path();
                initPaint();
                break;
            case 2:
                this.mDrawPath.lineTo(touchX, touchY);
                break;
            default:
                return false;
        }
        invalidate();
        return true;
    }

}
