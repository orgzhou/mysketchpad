package com.example.mysketchpad.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;

/**
 * Created by zhou on 2020/2/9 11:18.
 */
public class MyHuaBanView extends View {

    private Context context;
    private Bitmap mBitmap;
    private Canvas mCanvas;
    private Path mPath;
    private Paint mBitmapPaint; //画布的画笔
    private Paint mPaint; //真实的画笔
    private float mX,mY; //临时点坐标
    private static final float Touch_Tolerance = 4;
    //保存Path路径的集合
    private static List<DrawPath> savePath;
    //保存已经删除的Path的集合
    private static List<DrawPath> deletePath;
    //记录Path路径的对象 绘制路径
    private DrawPath drawPath;

    private int screenWidth,screenHeight;
    private int currentColor = Color.BLACK;
    private int currentSize = 5;
    private int currentStyle = 1;
    private int[] paintColor;//颜色集合


    public MyHuaBanView(Context context, int w, int h){
        super(context);
        this.context = context;
        screenWidth = w;
        screenHeight = h;
        paintColor = new int[]{
                Color.RED,Color.BLUE,Color.GREEN,
                Color.GRAY,Color.YELLOW,Color.CYAN
        };
        setLayerType(LAYER_TYPE_SOFTWARE,null);//设置默认样式，去除dis-in的黑色方框以及clear模式的黑线效果
        initCanvas();
        savePath = new ArrayList<DrawPath>();
        deletePath = new ArrayList<DrawPath>();
    }

    public void initCanvas(){
        setPaintStyle();
        mBitmapPaint = new Paint(Paint.DITHER_FLAG);
        //画布大小
        mBitmap = Bitmap.createBitmap(screenWidth,screenHeight, Bitmap.Config.ARGB_8888);
        mBitmap.eraseColor(Color.argb(0,0,0,0));
        mCanvas = new Canvas(mBitmap);  // 所有mCanvas画的东西都被保存在了mBitmap中
        mCanvas.drawColor(Color.TRANSPARENT);
    }

    //初始化画笔样式
    private void setPaintStyle(){
        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND); //外边缘
        mPaint.setStrokeCap(Paint.Cap.ROUND); //形状
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        if(currentStyle == 1){
            mPaint.setStrokeWidth(currentSize);
            mPaint.setColor(currentColor);
        }else{//橡皮擦
            mPaint.setAlpha(0);
            mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
            mPaint.setColor(Color.TRANSPARENT);
            mPaint.setStrokeWidth(50);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawBitmap(mBitmap,0,0,mBitmapPaint);
        if(mPath != null){
            canvas.drawPath(mPath,mPaint);
        }
    }

    private void touch_start(float x, float y){
        mPath.moveTo(x,y);
        mX = x;
        mY = y;
    }

    private void touch_move(float x, float y){
        float dx = Math.abs(x - mX);
        float dy = Math.abs(mY - y);
        if(dx >= Touch_Tolerance || dy >= Touch_Tolerance){

        }
    }

}
