package com.example.mysketchpad.view;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

/**
 * Created by zhou on 2020/3/5 15:03.
 */
public class SloopView extends View {

    //创建一个画笔
    private Paint mPaint = new Paint();

    public SloopView(Context context) {
        super(context);
    }

    public SloopView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initPaint();
    }

    public SloopView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    //初始化一个画笔
    private void initPaint(){
        mPaint.setColor(Color.BLACK); //设置画笔颜色
        mPaint.setStyle(Paint.Style.FILL); //设置画笔模式为填充
        mPaint.setStrokeWidth(10f); //设置画笔宽度为10px
    }

}
