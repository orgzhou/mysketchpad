package com.example.mysketchpad;

/**
 * Created by zhou on 2020/2/12 13:12.
 */
public class TestInfo {

    private String ImgPath;

    private String ImgName;

    public TestInfo(String imgPath, String imgName) {
        ImgPath = imgPath;
        ImgName = imgName;
    }

    public String getImgPath() {
        return ImgPath;
    }

    public void setImgPath(String imgPath) {
        ImgPath = imgPath;
    }

    public String getImgName() {
        return ImgName;
    }

    public void setImgName(String imgName) {
        ImgName = imgName;
    }
}
