package com.example.mysketchpad;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.blankj.utilcode.util.FileUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.mysketchpad.util.MySharedpreferencedUtils;
import com.example.mysketchpad.white.adapter.MineAdapter;
import com.example.mysketchpad.white.bean.Image;
import com.example.mysketchpad.white.bean.LocalBitmap;
import com.example.mysketchpad.white.bean.Note;
import com.example.mysketchpad.white.bean.NoteModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.kongzue.dialog.v3.CustomDialog;
import com.kongzue.titlebar.TitleBar;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * 是我的作品展示页面
 */
public class MineActivity extends AppCompatActivity {
    private static final String TAG = "MineActivity::";
    @BindView(R.id.note_title)
    TitleBar titleBar;
    @BindView(R.id.note_rv)
    RecyclerView noteRv;
    private MineAdapter adapters;
    private String filePath;
//    private List<Image> imageList = new ArrayList<>();
    private List<LocalBitmap> localBitmapList = new ArrayList<>();
    private int DetailImageCode = 2001;
    SPUtils spUtils = SPUtils.getInstance();

    //gson实例化
    GsonBuilder builder = new GsonBuilder();
    Gson gson = builder.create();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mine);
        ButterKnife.bind(this);
        //初始化gson对象

        //list是解析出来的数据
        localBitmapList = MySharedpreferencedUtils.getLocalData();
        if (localBitmapList == null || localBitmapList.isEmpty()) {
            return;
        }
//        LocalBitmap localBitmaps = gson.fromJson(mineModelGson, LocalBitmap.class);
        //布局管理器
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this,2);
        noteRv.setLayoutManager(gridLayoutManager);
        //获取完整路径
//        filePath = Environment.getExternalStorageDirectory().toString()+ File.separator+"mysketchpad";
////        Log.e("zjl",filePath);
//        // 得到该路径文件夹下所有的文件
//        File fileALL = new File(filePath);
////        Log.e("zjl","该路径文件夹下所有的文件:"+fileALL);
//        //listFiles()方法是返回某个目录下所有文件和目录的绝对路径，返回的是File数组
//        File[] files = fileALL.listFiles();
//        assert files != null;
//        //循环取出
//        for (int i = 0; i < files.length; i++) {
//            File file = files[i];
////            Log.e("zjl","存入实体类的："+file.getPath()+" 名称 ："+file.getName() + "用户输入的:" + localBitmaps.getLocalInputName());
////            Image img = new Image(file.getPath(),file.getName());
//            /// Log.v("qwe","file里的::" + localBitmaps.getLocalInputName());
//           // LocalBitmap localBitmap = new LocalBitmap(file.getName(), localBitmaps.getLocalInputName(),file.getPath());
//
//            //localBitmapList.add(localBitmap);
////            Log.v("zjl","localBitmapList::" + localBitmapList.toString());
//        }
        adapters = new MineAdapter(MineActivity.this,R.layout.item_note,localBitmapList);
        noteRv.setAdapter(adapters);
        adapters.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
//                Image image = adapters.getItem(position);
                Bundle bundle = new Bundle();
                Log.e("jianli","adapter putString : " + adapters.getData().get(position).getLocalFilePath());
                //放入本地图片的完整路径
                bundle.putString("md_image_path",adapters.getData().get(position).getLocalFilePath());
                //放入点击的下标
                bundle.putInt("md_image_position",position);

                startActivityForResult(new Intent(MineActivity.this,MineDetailsActivity.class)
                        .putExtras(bundle),DetailImageCode);

            }
        });

        adapters.setOnItemLongClickListener(new BaseQuickAdapter.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(BaseQuickAdapter adapter, View view, int position) {
                Log.e("zjl","longClick ::" + adapters.getData().get(position).getLocalFilePath());
                Log.e("zjl","longClick 的所有数据 ::" + adapters.getData().get(position));
                Log.e("zjl","longClick 的 下标 ::" + position);
                ShowReName(position);
                return true;
            }
        });

    }

    //给item重新命名
    public void ShowReName(int position){
        CustomDialog.show(MineActivity.this, R.layout.layout_mine_rename_dialog, new CustomDialog.OnBindView() {
            @Override
            public void onBind(CustomDialog dialog, View v) {
                ImageView imgOk = v.findViewById(R.id.btn_ok);
                ImageView imgNo = v.findViewById(R.id.btn_no);
                EditText etReName = v.findViewById(R.id.mine_et_rename);

                imgOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(TextUtils.isEmpty(etReName.getText())){
                            ToastUtils.showShort("输入不能为空");
                            return;
                        }else{
                            //获取新的输入的名称
                            String newInputName = etReName.getText().toString();
                            //list获取下标并设置名称
                            localBitmapList.get(position).setLocalInputName(newInputName);
                            //转成json
                            Type type=new TypeToken<List<LocalBitmap>>(){}.getType();
                            String value = gson.toJson(localBitmapList,type);
                            //重新放入sp
                            SPUtils.getInstance().put("GsonLocalBitmap", value);
                            Log.e("zjl","重命名后 sp的值::" + value);
                            //通知刷新
                            adapters.notifyItemChanged(position);
                            dialog.doDismiss();
                        }
//                        String newName = etReName.getText().toString() + ".png";
//                        FileUtils.rename(filePath,newName);
//                        Image item = adapters.getItem(position);
//                        //先修改name
//                        item.setImageTitle(newName);
//                        //然后获取旧的path
//                        String oldPath = item.getImagePath();
//                        File file = new File(oldPath);
//                        //在获取旧filepath里面的name
//                        String oldName = file.getName();
//                        //然后replace
//                        String newPath = oldPath.replace(oldName, newName);
//                        item.setImagePath(newPath);
//
//                        adapters.notifyItemChanged(position);
//                        dialog.doDismiss();
                    }
                });

                imgNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.doDismiss();
                    }
                });

            }
        }).setCancelable(false).setAlign(CustomDialog.ALIGN.DEFAULT);
    }


    //这个回调是删除item的回调
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
            if(requestCode == DetailImageCode){

                assert data != null;
                //获取上个页面回调的path
                String resultPath = data.getStringExtra("mdImageUrl");
                //获取上个页面回调的position
                String resultPosition = data.getStringExtra("mdImagePosition");
                assert resultPosition != null;
                int position = Integer.parseInt(resultPosition);
                assert resultPath != null;
//                ToastUtils.showShort("md返回删除的路径" + resultPath);
//                Log.e("zjl","md返回删除的位置" + position);
//                Log.e("zjl","md返回删除的路径" + resultPath);
//                Image image = adapters.getItem(position);
                //删除路径
                FileUtils.delete(resultPath);
                //list也删除对应下标的数据
                localBitmapList.remove(position);
                //然后再转成json
                Type type=new TypeToken<List<LocalBitmap>>(){}.getType();
                String value = gson.toJson(localBitmapList,type);
                //重新放入sp
                SPUtils.getInstance().put("GsonLocalBitmap", value);
                Log.e("zjl","回调删除后 sp的值::" + value);
                //通知刷新
                adapters.notifyItemRemoved(position);
                adapters.notifyItemRangeRemoved(0,adapters.getItemCount());


            }
        }
    }

    public void getData(){
        NoteModel.getInstance().getNoteList(new NoteModel.NoteCallback() {
            @Override
            public void onCallback(List<Note> data) {
                if(data.size() > 0){
//                    getView().showContent();
//                    Collections.reverse(data);
//                    setData(data);
                    Log.e("zjl","getNoteList" + data.toString());
//                    list = data;
//                    adapter = new MineAdapter(MineActivity.this,R.layout.item_note,data);
//                    noteRv.setAdapter(adapter);
                }
            }
        });
    }

}
