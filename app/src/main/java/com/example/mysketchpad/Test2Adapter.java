package com.example.mysketchpad;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.List;

import androidx.annotation.Nullable;

/**
 * Created by zhou on 2020/2/12 13:11.
 */
public class Test2Adapter extends BaseQuickAdapter<TestInfo, BaseViewHolder> {

    private Context context;

    public Test2Adapter(Context context, int layoutResId, @Nullable List<TestInfo> data) {
        super(layoutResId, data);
        this.context =context;
    }

//    @Override
//    protected void convert(BaseViewHolder helper, String item) {
//        Glide.with(context).load(item).into((ImageView) helper.getView(R.id.iv_broken));
//    }

    @Override
    protected void convert(BaseViewHolder helper, TestInfo item) {
        helper.setText(R.id.tv_broken,item.getImgName());
        Glide.with(context).load(item.getImgPath()).into((ImageView) helper.getView(R.id.iv_broken));
    }
}
