package com.example.mysketchpad;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.content.Intent;
import android.icu.text.CaseMap;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;

/**
 * 我的作品 单个展示大图页面
 */
public class MineDetailsActivity extends AppCompatActivity {

    @BindView(R.id.md_toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_title)
    TextView tvTitle;
    @BindView(R.id.md_image)
    ImageView imgContent;
    Bundle bundle;
    String mdImageUrl;
    int mdImagePosition;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mine_details);
        ButterKnife.bind(this);
        initToolbar();
        initView();
    }

    private void initView() {
        bundle = getIntent().getExtras();
        mdImageUrl = bundle.getString("md_image_path");
        mdImagePosition = bundle.getInt("md_image_position");
        //加载图
        Glide.with(MineDetailsActivity.this)
                .load(mdImageUrl).into(imgContent);

    }

    private void initToolbar() {
        tvTitle.setText("作品详情");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.mine_details,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.jixubianji:
                ToastUtils.showShort("1");
                break;

            case R.id.fenxiang:
                ToastUtils.showShort("2");

                break;

            case R.id.shanchu:
//                ToastUtils.showShort("3");
                Intent intent = new Intent();
                intent.putExtra("mdImageUrl",mdImageUrl);
                intent.putExtra("mdImagePosition",mdImagePosition+"");
                setResult(RESULT_OK,intent);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
