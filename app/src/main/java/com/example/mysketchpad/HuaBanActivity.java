package com.example.mysketchpad;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.SeekBar;
import android.widget.TextView;

import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.mysketchpad.white.BoardView;
import com.example.mysketchpad.white.InputDialog;
import com.example.mysketchpad.white.ShapeResource;
import com.example.mysketchpad.white.adapter.HBColorAdapter;
import com.example.mysketchpad.white.bean.LocalBitmap;
import com.example.mysketchpad.white.bean.MineModel;
import com.example.mysketchpad.white.bean.Note;
import com.example.mysketchpad.white.bean.NoteModel;
import com.example.mysketchpad.white.shape.DrawShape;
import com.example.mysketchpad.white.Type;
import com.kongzue.dialog.v3.CustomDialog;
import com.kongzue.titlebar.TitleBar;
import com.kongzue.titlebar.interfaces.OnRightButtonPressed;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * 画板页面
 */
public class HuaBanActivity extends AppCompatActivity {
    private static final String TAG = "BoardView::";
    @BindView(R.id.huabi)
    ImageView huabi;
    @BindView(R.id.daxiao)
    ImageView daxiao;
    @BindView(R.id.xiangpica)
    ImageView xiangpica;
    @BindView(R.id.yanse)
    ImageView yanse;
    @BindView(R.id.chehui)
    ImageView chehui;
    @BindView(R.id.qingkong)
    ImageView qingkong;
    @BindView(R.id.buttonzz)
    LinearLayout bottomzz;
//    @BindView(R.id.beijing_view)
//    View beijing_view;
    private TitleBar titleBar;
    private BoardView boardView;
    private boolean isShowingSizeSelector = false;
    private boolean isShowingColorSelector = false;
    private PopupWindow mSizeWindow;
    private PopupWindow mColorWindow;
    private CustomDialog customDialog;
    InputDialog inputDialog;
    private Note mLocalNote;

    public void setLocalNoteNull(){ mLocalNote = null;}

    public Note getLocalNote(){ return mLocalNote;}

    SPUtils spUtils = SPUtils.getInstance();
    //这里就是 用了个数组装
    public static final Integer[] COLORS = {
            new Integer(Color.BLACK), new Integer(0xFFFFC125),new Integer(0xFFFF82AB),new Integer(0xFFFFD39B),
            new Integer(0xFFFFBBFF),new Integer(0xFF7FFFD4),new Integer(0xFF7D26CD),new Integer(0xFFC0FF3E),
            new Integer(0xFFFF8247),new Integer(0xFFFF34B3),new Integer(0xFFFF0000),new Integer(0xFFFF00FF),
            new Integer(0xFFFF6A6A),new Integer(0xFFFF4500),new Integer(0xFFC1CDCD),new Integer(0xFFC6E2FF),
            new Integer(0xFF32CD32),new Integer(0xFF228B22),new Integer(0xFF191970),new Integer(0xFF9B30FF),
            new Integer(0xFF8B0A50),new Integer(0xFF8B3A3A),new Integer(0xFF8B7500),new Integer(0xFFCCCCCC),
            new Integer(0xFF218868),new Integer(0xFF2F4F4F),new Integer(0xFFCD5B45),new Integer(0xFFEE7600)
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hua_ban);
        ButterKnife.bind(this);
        boardView = findViewById(R.id.board_view);
//        boardView = new BoardView();
        titleBar = findViewById(R.id.titleBar);
        titleBar.setOnRightButtonPressed(new OnRightButtonPressed() {
            @Override
            public void onRightButtonPressed(View v) {
//                ToastUtils.showShort("");
                if(boardView.getNotePath() == null || boardView.getNotePath().size() == 0){
                    ToastUtils.showShort("请先绘画,才可以保存");
                    return;
                }else{
                    showNoteDialog();
                }

            }
        });
    }


//    public void setBg(){
//        int colorBg = spUtils.getInt("bgcolors");
//        beijing_view.setBackgroundColor(colorBg);
//        boardView.setBeijingyanse(colorBg);
//    }

    @OnClick({R.id.huabi,R.id.daxiao,R.id.xiangpica,R.id.yanse,R.id.chehui,R.id.qingkong})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.huabi:
                boardView.setDrawType(Type.CURVE);
                break;
            case R.id.daxiao:
                if(isShowingSizeSelector){
                    mSizeWindow.dismiss();
                    isShowingSizeSelector = false;
                }else{
                    showSizeSelectorWindow();
                }
                break;

            case R.id.xiangpica:
                boardView.setDrawType(Type.WIPE);
                break;
            case R.id.yanse:
                showColorSelectDialog();
                break;
            case R.id.chehui:
                boardView.reCall();
                break;
            case R.id.qingkong:
                ShowIsClear();
                break;
        }
    }

    //保存路径弹框
    public void showNoteDialog(){
        inputDialog = new InputDialog(this);
        inputDialog.setCancelable(false);
        inputDialog.setHint("请输入！");
        if(getLocalNote() != null){
            inputDialog.setContent(getLocalNote().TitleNmae);
        }
        inputDialog.show();
        inputDialog.setNoteImgYesClickListener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //保存标题 和 绘画路径
//                saveNote(inputDialog.getContent(),boardView.getNotePath());
//                saveImageToGallery(boardView.getDrawBitmap());
                if(TextUtils.isEmpty(inputDialog.getContent())){
                    ToastUtils.showShort("名称不能为空,请重新输入");
                    return;
                }else{
                    Log.e("zjl","点击事件里面的::  "+boardView.getDrawBitmap());
                    //保存图片和标题
                    saveImage(boardView.getDrawBitmap(),inputDialog.getContent());
                    //保存绘画路径和标题
                    saveNote(inputDialog.getContent(),boardView.getNotePath());

                    inputDialog.dismiss();
                }
            }
        });
        inputDialog.setNoteImgNoClickListener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ToastUtils.showShort("取消保存");
                inputDialog.dismiss();
            }
        });
    }

    public void saveNote(String title, List<ShapeResource> paths){
        if(TextUtils.isEmpty(title)){
            ToastUtils.showShort("标题不能为空");
            return;
        }
        Note note = new Note();
        note.TitleNmae = title;
        note.mPaths = paths;
        long time = System.currentTimeMillis();
        note.CreateTime = time;
        note.mFileName = time+"";
//        note.imgPath =
        Log.e("zjl","重要保存的 名称----"+note.TitleNmae);
        Log.e("zjl","重要保存的 time----"+note.mFileName);
        Log.e("zjl","重要保存的 path----"+note.mPaths);

        NoteModel.getInstance().saveNote(note);

        Log.e("zjl","方法调用后->保存的 名称 ----"+note.TitleNmae);
        Log.e("zjl","方法调用后->保存的 time ----"+note.mFileName);
        Log.e("zjl","方法调用后->保存的 path ----"+note.mPaths);
        Log.e("zjl","方法调用后->保存的 note ----"+note.toString());


        if(mLocalNote != null){
            NoteModel.getInstance().deleteNoteFile(mLocalNote.mFileName);
            mLocalNote = null;
        }
    }


    public void saveImage(Bitmap b,String title){
        if(TextUtils.isEmpty(title)){
            ToastUtils.showShort("标题不能为空");
            return;
        }else{

            MineModel.getInstance().saveCurveToApp(b,title);
        }

    }

    //保存图片到本地
    public static void saveImageToGallery(Bitmap bmp) {
        // 首先保存图片
        File appDir = new File(Environment.getExternalStorageDirectory(), "MYSKT");
        Log.e("zjl",appDir+"");
        if (!appDir.exists()) {
            appDir.mkdir();
        }
        String fileName = "hahazzz"+System.currentTimeMillis() + ".jpg";
        File file = new File(appDir, fileName);
        try {
            FileOutputStream fos = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.e("zjl",""+file.getAbsolutePath());
    }


    public void ShowIsClear(){
        CustomDialog.show(HuaBanActivity.this, R.layout.layout_custom_dialog, new CustomDialog.OnBindView() {
            @Override
            public void onBind(CustomDialog dialog, View v) {
                ImageView imgOk = v.findViewById(R.id.btn_ok);
                ImageView imgNo = v.findViewById(R.id.btn_no);


                imgOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        clear();
                        dialog.doDismiss();
                    }
                });

                imgNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.doDismiss();
                    }
                });

            }
        }).setCancelable(false).setAlign(CustomDialog.ALIGN.DEFAULT);
    }

    public void clear(){
        boardView.clearScreen();
    }

    //设置画笔大小
    public void showSizeSelectorWindow(){
        if(isShowingSizeSelector){
            return;
        }else if(isShowingColorSelector){
            mColorWindow.dismiss();
            isShowingColorSelector = false;
        }
        isShowingSizeSelector = true;
        View view = LayoutInflater.from(this).inflate(R.layout.main_window_size_selector,null);
        SeekBar seekBar = view.findViewById(R.id.seek_bar);
        TextView size = view.findViewById(R.id.size);
        int numSize = (int) DrawShape.mPaintWidth;
        seekBar.setProgress(numSize);
        size.setText(numSize+"");
        mSizeWindow = new PopupWindow(view, ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        int measuredWidth = view.getMeasuredWidth();
        int measuredHeight = view.getMeasuredHeight();
        mSizeWindow.setBackgroundDrawable(new ColorDrawable(0xaa000000));//设置背景
        mSizeWindow.setOutsideTouchable(true);//点击外面窗口消失

        //获取点击View的坐标
        int[] location = new int[2];
        bottomzz.getLocationOnScreen(location);
        mSizeWindow.showAtLocation(daxiao,  Gravity.NO_GRAVITY, (location[0] + bottomzz.getWidth() / 2) - measuredWidth / 2, location[1]-measuredHeight);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                size.setText(progress + "");
                DrawShape.mPaintWidth = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mSizeWindow.dismiss();
                isShowingSizeSelector = false;
            }
        });
    }

    public void showColorSelectDialog(){
//        if(isShowingColorSelector){
//            return;
//        }else if(isShowingSizeSelector){
//
//            isShowingSizeSelector = false;
//            customDialog.doDismiss();
//        }
//        isShowingColorSelector = true;
        customDialog.show(HuaBanActivity.this, R.layout.layout_color_dialog, new CustomDialog.OnBindView() {
            @Override
            public void onBind(CustomDialog dialog, View v) {
//                Color.parseColor()
                RecyclerView recyclerView = v.findViewById(R.id.rv_color);
                recyclerView.setLayoutManager(new GridLayoutManager(HuaBanActivity.this,6));
                List<Integer> listColor = Arrays.asList(COLORS);
                HBColorAdapter hbColorAdapter = new HBColorAdapter(HuaBanActivity.this,R.layout.item_huaban_color,listColor);
                recyclerView.setAdapter(hbColorAdapter);
                hbColorAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                        DrawShape.mPaintColor = (int) adapter.getData().get(position);
//                        BoardView.setBgColor((int) adapter.getData().get(position));
                        dialog.doDismiss();
                        Log.e("zjl",""+adapter.getData().get(position));
                    }
                });

//                hbColorAdapter.setOnItemChildLongClickListener(new BaseQuickAdapter.OnItemChildLongClickListener() {
//                    @Override
//                    public boolean onItemChildLongClick(BaseQuickAdapter adapter, View view, int position) {
//                        int bgcolors = (int) adapter.getData().get(position);
//                        spUtils.put("bgcolors",bgcolors);
//                        dialog.doDismiss();
//                        return false;
//                    }
//                });
                //这里 长按 选择 画布的背景颜色
                hbColorAdapter.setOnItemLongClickListener(new BaseQuickAdapter.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(BaseQuickAdapter adapter, View view, int position) {
                        Log.i(TAG, "onItemLongClick: ");
                        //int值的颜色
                        int bgcolors = (int) adapter.getData().get(position);
                        //sp 存储进去
                        spUtils.put("bgcolors",bgcolors);
                        Log.e("zjl",""+bgcolors);
//                        boardView.mPaintBgColor = bgcolors;
//                        boardView.invalidate();
//                        boardView.setBgColor(bgcolors);
//                        setBg();
//                        boardView.setBeijingyanse(bgcolors);
                        dialog.doDismiss();
                        return false;
                    }
                });
            }
        }).setCancelable(false).setAlign(CustomDialog.ALIGN.DEFAULT);

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
//        spUtils.remove("bgcolors");
    }
}
