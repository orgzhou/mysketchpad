package com.example.mysketchpad;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class Main2Activity extends AppCompatActivity {
    ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        imageView = findViewById(R.id.img22);
        Bundle bundle = getIntent().getExtras();
        String imageUrl = bundle.getString("pp");
        Glide.with(this).load(imageUrl).into(imageView);
    }
}
