package com.example.mysketchpad.white;

import java.io.Serializable;

/**
 * Created by zhou on 2020/2/26 13:27.
 */
public class ColorBean implements Serializable {

    public int colorValue;

    public ColorBean(int colorValue) {
        this.colorValue = colorValue;
    }

    public int getColorValue() {
        return colorValue;
    }

    public void setColorValue(int colorValue) {
        this.colorValue = colorValue;
    }
}
