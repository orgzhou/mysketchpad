package com.example.mysketchpad.white;

/**
 * Created by zhou on 2020/2/12 20:39.
 */
public interface Type {

    int CURVE = 123;

    int RECTANGLE = 124;

    int OVAL = 125;

    int LINE = 126;

    int WIPE = 127;

    int MULTI_LINE = 128;

}
