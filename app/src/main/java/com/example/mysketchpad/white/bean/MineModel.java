package com.example.mysketchpad.white.bean;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.Toast;

import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.example.mysketchpad.util.AsyncThreadPool;
import com.example.mysketchpad.util.MySharedpreferencedUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhou on 2020/3/3 18:31.
 */
public class MineModel extends SuperModel {

    SPUtils spUtils = SPUtils.getInstance();

    private File appDir;

    public MineModel() {
        appDir = new File(Environment.getExternalStorageDirectory(), "mysketchpad");
        if (!appDir.exists()) {
            appDir.mkdir();
        }
    }

    public static MineModel getInstance() {
        return getInstance(MineModel.class);
    }



    //保存图片到本地
    public void saveCurveToApp(final Bitmap bitmapResource, String bitmapTitle) {
        //用户输入标题 = 传入的
        String localInputName = bitmapTitle;
//        Log.v("zjl","保存时候的名称::" + localInputName);
        long time = System.currentTimeMillis();
        //文件名称就是 时间+""
        String localFileNmae = time+"";
        //只是文件夹路径就是 appDir + getAbsolutePath
        String localFilePath = appDir.getAbsolutePath();

        //文件完整路径就是 filePath + 输入的名称 + 保存时间
        File imgageFile = new File(localFilePath, localInputName+localFileNmae +".png");
        // 保存的时候是啥路径，展示就是傻路径，你之前保存在本地的路径和最终展示的路径跟不上就不一致
        MySharedpreferencedUtils.saveToLocal(getContext(), new LocalBitmap(localFileNmae,localInputName,imgageFile.getAbsolutePath()));

        Log.e("zjl","完整路径::" + imgageFile);
        if (bitmapResource.getByteCount() == 0) {
            ToastUtils.showShort("bitmap为空,");
            return;
        }
        AsyncThreadPool.getInstance().executeRunnable(new Runnable() {
            @Override
            public void run() {

                try {
                    FileOutputStream out = new FileOutputStream(imgageFile);
                    bitmapResource.compress(Bitmap.CompressFormat.PNG, 100, out);
                    out.flush();
                    out.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    public File getAppImageDir() {
        return appDir;
    }

}
