package com.example.mysketchpad.white;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mysketchpad.R;

import androidx.appcompat.app.AppCompatDialog;

/**
 * Created by zhou on 2020/2/29 20:56.
 */
public class InputDialog extends AppCompatDialog {

    private Context context;

    private TextView noteTitle;
    private EditText noteInputEt;
    private ImageView noteImgYes,noteImgNo;

    public InputDialog(Context context) {
        this(context, R.style.MaterialDialogStyle);
    }

    public InputDialog(Context context, int theme) {
        super(context, theme);
        this.context = context;
        initView();
    }


    protected InputDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        setCancelable(cancelable);
        setOnCancelListener(cancelListener);
    }


    private void initView() {
        View dialogContent = LayoutInflater.from(context).inflate(R.layout.layout_note_dialog,null);
        noteTitle = dialogContent.findViewById(R.id.title_note);
        noteInputEt = dialogContent.findViewById(R.id.input_content_note);
        noteImgYes = dialogContent.findViewById(R.id.btn_ok_note);
        noteImgNo = dialogContent.findViewById(R.id.btn_no_note);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(dialogContent,
                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        setNoteImgNoClickListener(null);
    }

    //确认按钮
    public void setNoteImgYesClickListener(final OnClickListener listener){
        noteImgYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener != null){
                    listener.onClick(InputDialog.this,0);
                }
            }
        });
    }

    //取消按钮
    public void setNoteImgNoClickListener(final OnClickListener listener){
        noteImgNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onClick(InputDialog.this, 1);
                }
            }
        });
    }


    public String getContent(){
        return noteInputEt.getText().toString();
    }

    public EditText getInputView(){
        return noteInputEt;
    }

    public void setContent(String content){
        noteInputEt.setText(content);
    }

    public void setHint(String content){
        noteInputEt.setHint(content);
    }

    public ImageView getYesButton(){
        return noteImgYes;
    }
    public ImageView getNoButton(){
        return noteImgNo;
    }

}
