package com.example.mysketchpad.white;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.mysketchpad.R;
import com.example.mysketchpad.Test3Activity;

import java.util.Collections;
import java.util.List;
import java.util.PrimitiveIterator;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by zhou on 2020/2/26 14:04.
 */
public class ColorJlAdapter extends RecyclerView.Adapter<ColorJlAdapter.ViewHolder> {

    private Context mContext;
    private LayoutInflater mInflater;
    private List<Integer> mList;

    public ColorJlAdapter(Context mContext, List<Integer> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_color_jilu,parent,false);
        ViewHolder holder = new ViewHolder(itemView);
        return holder;


    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
//        mList.get(position);
        holder.colorView.setColor(mList.get(position),true);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


//    private DetailViewHolderListener mListener;

//    public ColorJlAdapter(Context context, List<Integer> list, DetailViewHolderListener mListener) {
//        this.mContext = context;
//        this.mList = list;
//        this.mInflater = LayoutInflater.from(context);
//        this.mListener = mListener;
//    }

//
//    @Override
//    public int getCount() {
//        return mList.size();
//    }
//
//    @Override
//    public Object getItem(int position) {
//        return mList.get(position);
//    }
//
//    @Override
//    public long getItemId(int position) {
//        return position;
//    }
//
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        ViewHolder holder = null;
//        if (convertView == null) {
//            holder = new ViewHolder();
//            convertView = mInflater.inflate(R.layout.item_color_jilu, null);
//            holder.colorView =  convertView.findViewById(R.id.item_color_jilus);
////            holder.tv_spmc = (TextView) convertView.findViewById(R.id.tv_spmc);
//
//            convertView.setTag(holder);
//        } else {
//            holder = (ViewHolder) convertView.getTag();
//        }
//        mListener.setData(holder, position);
//        return convertView;
//    }
//
//    public class ViewHolder {
////        public TextView tv_spbh;public TextView tv_spmc;
////        public TextView tv_cw;
////        public RoundButton btn_update;
//        public ColorView colorView;
//    }
//    /**
//     * 展示不同数据的接口
//     */
//    public interface DetailViewHolderListener {
//        void setData(ViewHolder viewHolder, int position);
//    }
    public class ViewHolder extends RecyclerView.ViewHolder {
//        public TextView tv_spbh;public TextView tv_spmc;
//        public TextView tv_cw;
//        public RoundButton btn_update;
        public ColorView colorView;

            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                colorView =  itemView.findViewById(R.id.item_color_jilus);
            }
        }
}
