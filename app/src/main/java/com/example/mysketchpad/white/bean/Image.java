package com.example.mysketchpad.white.bean;

import java.io.Serializable;

/**
 * Created by zhou on 2020/3/3 23:12.
 */
public class Image implements Serializable {

    public String imagePath; //路径

    public String imageTitle; //名称

    public String inputName; //用户输入的名称 做展示 修改 用的

    public Image(String imagePath, String imageTitle, String inputName) {
        this.imagePath = imagePath;
        this.imageTitle = imageTitle;
        this.inputName = inputName;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getImageTitle() {
        return imageTitle;
    }

    public void setImageTitle(String imageTitle) {
        this.imageTitle = imageTitle;
    }

    public String getInputName() {
        return inputName;
    }

    public void setInputName(String inputName) {
        this.inputName = inputName;
    }
}
