package com.example.mysketchpad.white;

import android.graphics.Path;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.PrimitiveIterator;
import java.util.PriorityQueue;

/**
 * Created by zhou on 2020/2/13 16:17.
 */
//可写路径
public class WritablePath extends Path implements Serializable {

    private static final long serialVersionUID = 1L;

    public WritablePaint mPaint;

    private ArrayList<float[]> mPathPoints;

    public WritablePath(){
        super();
        mPathPoints = new ArrayList<>();
    }

    //添加路径点
    public void addPathPoints(float[] points){
        mPathPoints.add(points);
    }

    //将路径点加载为QuadTo
    public void loadPathPointsAsQuadTo(){
        float[] initPoints = mPathPoints.remove(0);
        moveTo(initPoints[0], initPoints[1]);
        for(float[] points : mPathPoints){
            quadTo(points[0],points[1],points[2],points[3]);
        }
    }

}
