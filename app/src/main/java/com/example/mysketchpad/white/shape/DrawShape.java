package com.example.mysketchpad.white.shape;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.example.mysketchpad.view.DrawPath;
import com.example.mysketchpad.white.WritablePaint;

/**
 * Created by zhou on 2020/2/12 21:17.
 */
public abstract class DrawShape {

    protected WritablePaint mPaint;
    protected int mStartX;
    protected int mStartY;
    protected int mEndX;
    protected int mEndY;

    public void setYanse(int yanse){
        mPaint.setColor(yanse);
    }

    public static int mPaintColor = Color.BLACK;
    public static float mPaintWidth = 5f;
//    public static int mPaintBgColor = Color.WHITE;

    public DrawShape(){
        mPaint = new WritablePaint();
        mPaint.setAntiAlias(true);
        mPaint.setColor(mPaintColor);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(mPaintWidth);
        mPaint.setDither(true);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
    }

    public void touchDown(int startX, int startY){
        mStartX = startX;
        mStartY = startY;
    }

    public void touchUp(int endX, int endY){
        mEndX = endX;
        mEndY = endY;
    }

    public abstract void touchMove(int currentX, int currentY);

    public abstract void draw(Canvas canvas);

    public WritablePaint getPaint(){
        mPaint.mColor = mPaintColor;
        mPaint.mWidth = mPaintWidth;
        return mPaint;
    }

    public int getStartX(){
        return mStartX;
    }

    public int getStartY(){
        return mStartY;
    }

    public int getEndX(){return mEndX;}
    public int getEndY(){return mEndY;}

}
