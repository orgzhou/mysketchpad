package com.example.mysketchpad.white.bean;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.blankj.utilcode.util.ToastUtils;
import com.example.mysketchpad.util.AsyncThreadPool;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhou on 2020/3/2 13:50.
 */
public class NoteModel extends SuperModel{

    private Handler mHandler;

    public NoteModel(){
        mHandler = new Handler(Looper.getMainLooper());
    }

    public static NoteModel getInstance(){
        return getInstance(NoteModel.class);
    }

    public void saveNote(Note note){
        if(note.mPaths.size() == 0){
            ToastUtils.showShort("笔记不能为空");
        }else{
            putObject(note.mFileName,note);
            Log.e("zjl","putObject进去的值："+ note.toString());
        }
    }

    //获取笔迹
    public void getNoteList(final NoteCallback noteCallback) {
        AsyncThreadPool.getInstance().executeRunnable(new Runnable() {
            @Override
            public void run() {
                final List<Note> notes = new ArrayList<>();
                File cacheDir = new File(getObjectCacheDir());
                Log.e("zjl","cacheDir"+ cacheDir );
                if (!cacheDir.exists()) {
                    ToastUtils.showShort("缓存目录不存在");
                } else {
                    File[] files = cacheDir.listFiles();
                    for (File f : files) {
                        notes.add((Note) readObjectFromFile(f));
                    }
                }
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        noteCallback.onCallback(notes);
                    }
                });
            }
        });
    }

    public void getImageList(final ImageCallback callback) {
        AsyncThreadPool.getInstance().executeRunnable(new Runnable() {
            @Override
            public void run() {
                final List<String> filePaths = new ArrayList<>();
                File cacheDir = CurveModel.getInstance().getAppImageDir();
                if (!cacheDir.exists()) {
                    ToastUtils.showShort("缓存目录不存在");
                } else {
                    File[] files = cacheDir.listFiles();
                    for (File f : files) {
                        filePaths.add(f.getAbsolutePath());
                    }
                }
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        callback.onCallback(filePaths);
                    }
                });
            }
        });
    }


    public void deleteNoteFile(final String fileName){
        AsyncThreadPool.getInstance().executeRunnable(new Runnable() {
            @Override
            public void run() {
                File cacheDir = new File(getObjectCacheDir());
                if (!cacheDir.exists()) {
                    ToastUtils.showShort("缓存目录不存在");
                } else {
                    File[] files = cacheDir.listFiles();
                    for (File f : files) {
                        if(f.getName().equals(fileName)){
                            f.delete();
                            return;
                        }
                    }
                }
            }
        });
    }



    private Object readObjectFromFile(File objectFile) {
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(objectFile));
            Object result = ois.readObject();
            ois.close();
            return result;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            Log.i("SuperModel", "对象缓存读取失败");
        }
        return null;
    }

    public interface NoteCallback{
        void onCallback(List<Note> data);
    }

    public interface ImageCallback{
        void onCallback(List<String> data);
    }

}
