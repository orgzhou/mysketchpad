package com.example.mysketchpad.white;

import android.content.Context;
import android.util.Log;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.mysketchpad.R;
import com.example.mysketchpad.Test3Activity;
import com.example.mysketchpad.white.shape.DrawShape;

import java.util.List;

/**
 * Created by zhou on 2020/2/24 21:26.
 */
public class ColorAdapter extends BaseQuickAdapter<Integer, BaseViewHolder> {

    private Context context;

    public ColorAdapter(Context context,int layoutResId,  List<Integer> data) {
        super(layoutResId, data);
        this.context = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, Integer item) {
        ColorView colorView = helper.getView(R.id.item_color);
        if(context instanceof Test3Activity && DrawShape.mPaintColor == item){
            Log.e("zjl",item+"");
            Log.e("zjl",DrawShape.mPaintColor+"");
            colorView.setColor(item,true);
        }else{
            colorView.setColor(item,false);
        }



    }
}
