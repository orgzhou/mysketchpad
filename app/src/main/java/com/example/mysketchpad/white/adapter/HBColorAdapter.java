package com.example.mysketchpad.white.adapter;

import android.content.Context;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.mysketchpad.HuaBanActivity;
import com.example.mysketchpad.R;
import com.example.mysketchpad.white.ColorView;
import com.example.mysketchpad.white.shape.DrawShape;
import com.example.mysketchpad.white.view.HBColorView;

import java.util.List;

import androidx.annotation.Nullable;

/**
 * Created by zhou on 2020/2/27 16:53.
 */
public class HBColorAdapter extends BaseQuickAdapter<Integer, BaseViewHolder> {

    private Context context;

    public HBColorAdapter(Context context, int layoutResId, @Nullable List<Integer> data) {
        super(layoutResId, data);
        this.context = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, Integer item) {
//        ColorView colorView = helper.getView(R.id.item_color);
        HBColorView hbColorView = helper.getView(R.id.hb_colorview);
        if(context instanceof HuaBanActivity && DrawShape.mPaintColor == item){
            hbColorView.setColor(item,true);
        }else{
            hbColorView.setColor(item,false);
        }
    }
}
