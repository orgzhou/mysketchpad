package com.example.mysketchpad.white.bean;

import android.content.Context;
import android.nfc.Tag;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.PrimitiveIterator;

/**
 * Created by zhou on 2020/3/1 16:22.
 */
public class SuperModel {

    private final String TAG = "SuperModel";
    private final String OBJECT_CACHE = "ObjectCachez";
    private String mObjectCachePath;
    private static Context mContext;
    private static Map<String, SuperModel> mInstanceMap = new HashMap<>();


    public SuperModel() {
        mObjectCachePath = mContext.getExternalFilesDir(OBJECT_CACHE).getAbsolutePath();
    }

    //这样去写单例模式虽然可以省去很多代码，不过因为newInstance方法有限制：构造函数必须public,必须有一个构造函数没有参数
    public static void initialize(Context context) {
        mContext = context;
    }


    public static <T extends SuperModel> T getInstance(Class<T> model) {
        if(!mInstanceMap.containsKey(model.getSimpleName())){
            synchronized (model){
                try {
                    T instance = model.newInstance();
                    mInstanceMap.put(model.getSimpleName(), instance);
                    return instance;
                } catch (InstantiationException e) {
                    e.printStackTrace();
                    return null;
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return (T) mInstanceMap.get(model.getSimpleName());
    }

    /**
     * 通过文件保存对象数据，对象必须可序列化。通过清理app数据即可清理掉数据
     * 目录:SDCard/Android/data/应用包名/data/files/ObjectCache
     * @param key
     * @param value
     */
    public void putObject(String key, Object value){
        File objectFile = new File(mObjectCachePath+"/"+key);
        Log.e("zjl","objectFile::" + objectFile);
        if(objectFile.exists()){
            objectFile.delete();
        }
        try {
            objectFile.createNewFile();
            ObjectOutputStream objectOutputStream
                     = new ObjectOutputStream(new FileOutputStream(objectFile));
            objectOutputStream.writeObject(value);
            objectOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
            Log.i(TAG, "文件流打开失败。");
        }
    }

    public Object getObject(String key) {
        File objectFile = new File(mObjectCachePath + "/" + key);
        if (!objectFile.exists()) {
            Log.i(TAG, "该对象没有被缓存");
            return null;
        }
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(objectFile));
            Object result = ois.readObject();
            ois.close();
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            Log.i(TAG, "对象缓存读取失败");
        }
        return null;
    }

    public void clearCacheObject() {
        File cacheDir = new File(mObjectCachePath);
        if (cacheDir.exists() && cacheDir.isDirectory()) {
            deleteDir(cacheDir);
        }
    }

    protected void deleteDir(File dir) {
        if (dir.isDirectory() && dir.listFiles().length > 0) {
            for (File file : dir.listFiles()) {
                deleteDir(file);
            }
        } else {
            dir.delete();
        }

    }

    public Context getContext(){
        return mContext;
    }

    public String getObjectCacheDir(){
        return mObjectCachePath;
    }

}
