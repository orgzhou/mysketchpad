package com.example.mysketchpad.white.bean;

import com.example.mysketchpad.white.ShapeResource;

import java.io.Serializable;
import java.util.List;

/**
 * Created by zhou on 2020/3/1 14:45.
 */
public class Note implements Serializable {

    public String mFileName;

    public String TitleNmae;

    public long CreateTime;

    public String imgPath;

    public String imgTitle;

    public List<ShapeResource> mPaths;

//    public String getImgPath() {
//        return imgPath;
//    }
//
//    public void setImgPath(String imgPath) {
//        this.imgPath = imgPath;
//    }
//
//    public String getImgTitle() {
//        return imgTitle;
//    }
//
//    public void setImgTitle(String imgTitle) {
//        this.imgTitle = imgTitle;
//    }
}
