package com.example.mysketchpad.white.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

/**
 * Created by zhou on 2020/2/27 17:25.
 */
public class HBColorView extends View {

    private int mWidth;
    private int mHeight;
    private Paint mCirclePaint;
    private Paint mCircle2Paint;
    private Paint mCircle3Paint;
    private boolean isChecked = false;

    public static int colorPaint = Color.WHITE;

    public HBColorView(Context context) {
        this(context,null);
    }

    public HBColorView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs,0);
    }

    public HBColorView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mCirclePaint = new Paint();
        mCirclePaint.setAntiAlias(true);
        mCirclePaint.setStyle(Paint.Style.FILL);

        mCircle2Paint = new Paint();
        mCircle2Paint.setStyle(Paint.Style.STROKE);
        mCircle2Paint.setColor(colorPaint);
        mCircle2Paint.setStrokeWidth(8f);
        mCircle2Paint.setAntiAlias(true);
        mCircle2Paint.setDither(true);
        mCircle2Paint.setStrokeJoin(Paint.Join.ROUND);
        mCircle2Paint.setStrokeCap(Paint.Cap.ROUND);

//        mCircle3Paint = new Paint();
//        mCircle3Paint.setStyle(Paint.Style.STROKE);
//        mCircle3Paint.setColor(Color.WHITE);
//        mCircle3Paint.setStrokeWidth(8f);
//        mCircle3Paint.setAntiAlias(true);
//        mCircle3Paint.setDither(true);
//        mCircle3Paint.setStrokeJoin(Paint.Join.ROUND);
//        mCircle3Paint.setStrokeCap(Paint.Cap.ROUND);

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        mWidth = MeasureSpec.getSize(widthMeasureSpec);
        mHeight = mWidth;
        setMeasuredDimension(mWidth, mHeight);
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float x = mWidth / 2;
        float y = mHeight / 2;
        float r = mWidth / 2;
        canvas.drawCircle(x, y, r, mCirclePaint);

        float x1 = mWidth / 2;
        float y1 = mHeight / 2;
        float r1 = mWidth / 2 - 4;

//        if (isChecked) {
//            colorPaint = Color.RED;
//            canvas.drawCircle(x1, y1, r1, mCircle2Paint);
//        }else{
//            canvas.drawCircle(x1, y1, r1, mCircle2Paint);
//        }
        canvas.drawCircle(x1, y1, r1, mCircle2Paint);
    }

    public void setColor(int color, boolean isChecked) {
        mCirclePaint.setColor(color);
        this.isChecked = isChecked;
        invalidate();
    }

    public void setChecked(boolean isChecked) {
        if (this.isChecked != isChecked) {
            this.isChecked = isChecked;
            invalidate();
        }
    }


}
