package com.example.mysketchpad.white.bean;

import java.io.Serializable;

/**
 * Created by zhou on 2020/3/8 15:39.
 */
public class LocalBitmap implements Serializable {

    public String localFileName;

    public String localInputName;

    public String localFilePath;

    public long localCreateTime;



    public LocalBitmap(String localFileName, String localInputName, String localFilePath) {
        this.localFileName = localFileName;
        this.localInputName = localInputName;
        this.localFilePath = localFilePath;
    }

    public String getLocalFileName() {
        return localFileName;
    }

    public void setLocalFileName(String localFileName) {
        this.localFileName = localFileName;
    }

    public String getLocalInputName() {
        return localInputName;
    }

    public void setLocalInputName(String localInputName) {
        this.localInputName = localInputName;
    }

    public String getLocalFilePath() {
        return localFilePath;
    }

    public void setLocalFilePath(String localFilePath) {
        this.localFilePath = localFilePath;
    }

    public long getLocalCreateTime() {
        return localCreateTime;
    }

    public void setLocalCreateTime(long localCreateTime) {
        this.localCreateTime = localCreateTime;
    }

    @Override
    public String toString() {
        return "LocalBitmap{" +
                "localFileName='" + localFileName + '\'' +
                ", localInputName='" + localInputName + '\'' +
                ", localFilePath='" + localFilePath + '\'' +
                ", localCreateTime=" + localCreateTime +
                '}';
    }
}
