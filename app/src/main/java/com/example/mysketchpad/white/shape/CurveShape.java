package com.example.mysketchpad.white.shape;

import android.graphics.Canvas;
import android.graphics.Rect;

import com.example.mysketchpad.white.WritablePath;

/**
 * Created by zhou on 2020/2/14 21:48.
 */
public class CurveShape extends DrawShape {

    private Rect mRect;
    protected WritablePath mPath;

    public CurveShape(){
        mPath = new WritablePath();
        mRect = new Rect();
    }

    @Override
    public void touchDown(int startX, int startY) {
        super.touchDown(startX, startY);
        //设置曲线开始点
        mPath.moveTo(startX,startY);
        float[] a  = {startX,startY,0,0};
        //添加初始路径点a
        mPath.addPathPoints(a);
    }

    @Override
    public void touchMove(int currentX, int currentY) {
        int border = (int) mPaint.getStrokeWidth();
        mRect.set(mStartX - border, mStartY - border,
                mStartX + border, mStartY + border);

        float mMiddleX = (currentX + mStartX) /2;
        float mMiddleY = (currentY + mStartY) /2;

        //贝塞尔曲线
        mPath.quadTo(mStartX,mStartY,mMiddleX,mMiddleY);

        float[] temp = {mStartX,mStartY,mMiddleX,mMiddleY};
        mPath.addPathPoints(temp);

        //重新计算rect的范围
        mRect.union((int) mMiddleX - border,
                (int)mMiddleY - border,
                (int)mMiddleX + border,
                (int)mMiddleY + border);

        mStartX = currentX;
        mStartY = currentY;
    }

    //把曲线绘制到画布上
    @Override
    public void draw(Canvas canvas) {
        canvas.drawPath(mPath,mPaint);
    }

    public WritablePath getPath(){
        mPaint.mColor = mPaintColor;
        mPaint.mWidth = mPaintWidth;
        mPath.mPaint = mPaint;
        return mPath;
    }

}
