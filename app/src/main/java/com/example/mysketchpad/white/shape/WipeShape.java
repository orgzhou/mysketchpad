package com.example.mysketchpad.white.shape;

import android.graphics.Color;

import com.example.mysketchpad.white.WritablePath;

/**
 * Created by zhou on 2020/2/15 20:36.
 */
public class WipeShape extends CurveShape{



    public WipeShape(){
        mPaint.setColor(Color.WHITE);
        mPaint.setStrokeWidth(100);
    }

    public WritablePath getPath(){
        mPaint.mColor = Color.WHITE;
        mPaint.mWidth = 100;
        mPath.mPaint = mPaint;
        return mPath;
    }

}
