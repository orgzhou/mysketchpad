package com.example.mysketchpad.white.adapter;

import android.content.Context;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.mysketchpad.R;
import com.example.mysketchpad.white.bean.Image;
import com.example.mysketchpad.white.bean.LocalBitmap;
import com.example.mysketchpad.white.bean.Note;

import java.io.File;
import java.util.List;

import androidx.annotation.Nullable;

/**
 * Created by zhou on 2020/3/2 22:56.
 */
public class MineAdapter extends BaseQuickAdapter<LocalBitmap, BaseViewHolder> {

    private Context context;

    public MineAdapter(Context context,int layoutResId, @Nullable List<LocalBitmap> data) {
        super(layoutResId, data);
        this.context = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, LocalBitmap item) {
        Log.v("zjl","adapter里的："+item.getLocalInputName());
        helper.setText(R.id.note_show_tv,item.getLocalInputName());
//        Log.e("zjl","adapter里的："+item.getLocalFilePath());
//        Log.w("zjl::", "convert: " + item.toString());
        Glide.with(context)
                .load(item.getLocalFilePath())
                .into((ImageView) helper.getView(R.id.note_show_img));
    }
}
