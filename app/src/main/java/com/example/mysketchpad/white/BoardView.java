package com.example.mysketchpad.white;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.blankj.utilcode.util.SPUtils;
import com.example.mysketchpad.MainActivity;
import com.example.mysketchpad.white.shape.CurveShape;
import com.example.mysketchpad.white.shape.DrawShape;
import com.example.mysketchpad.white.shape.LineShape;
import com.example.mysketchpad.white.shape.MultiLineShape;
import com.example.mysketchpad.white.shape.OvalShape;
import com.example.mysketchpad.white.shape.RectShape;
import com.example.mysketchpad.white.shape.WipeShape;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;

/**
 * Created by zhou on 2020/2/12 20:34.
 */
public class BoardView extends View {

    private static final String TAG = "BoardView::";
    private int mDrawType = Type.CURVE;
    private Bitmap mDrawBitmap;
    private Bitmap mDrawBitmap1;
    private Canvas mCanvas;
    private Paint mPaint; //渲染画布
    private Paint mPaint2;
    private DrawShape mShape; //绘图形状
    private int mWidth;
    private int mHeight;
    //是否清空屏幕
    private boolean isClearScreen = false;

//    public static int mPaintBgColor = Color.BLACK;
    //是否撤销或者撤回
    private boolean isRecentRecallOrUndo = false;

    SPUtils spUtils = SPUtils.getInstance();
    private OnDownAction mDownAction;
    private ArrayList<ShapeResource> mSavePath;
    private ArrayList<ShapeResource> mDeletePath;
    private int mBgColor,width,height;
//
//    public void setBgColor(int color) {
//        this.mBgColor = color;
//        invalidate();
//    }
//
//    public int getmBgColor() {
//        return mBgColor;
//    }
    public BoardView(Context context) {
        this(context,null);
    }

    public BoardView(Context context, AttributeSet attrs) {
        this(context, attrs,0);

    }

    public BoardView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        //防抖动的
        mPaint = new Paint(Paint.DITHER_FLAG);
        //保存路径
        mSavePath = new ArrayList<>();
        //撤回删除路径
        mDeletePath = new ArrayList<>();

//        int width = getMeasuredWidth();
//        int height = getMeasuredHeight();
//        mDrawBitmap = createBitmap(getWidth(), getHeight());

    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
//        int mWidths = getMeasuredWidth();
//        int mHeights = getMeasuredHeight();
//        if(mDrawBitmap == null){
//            mDrawBitmap = createBitmap(mWidths, mHeights);
//        }
}

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
//        mWidth = getMeasuredWidth();
//        mHeight = getMeasuredHeight();
//        mWidth = getMeasuredWidth();
//        mHeight = getMeasuredHeight();
//        mDrawBitmap = createBitmap(width, height);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
//        int width = getMeasuredWidth();
//        int height = getMeasuredHeight();
        mWidth = w;
        mHeight = h;
        Log.e("zjl","BoardView里Bitmap的宽和高:"+width+","+height);
        if(mDrawBitmap == null){
            mDrawBitmap = createBitmap(mWidth, mHeight);
        }

    }

    ////    private void drawBackground(Canvas canvas){
////        this.mPaint.setColor(this.mBackgroundColor);
////        this.mPaint.setStyle(Paint.Style.FILL);
////        canvas.drawRect(0.0f, 0.0f, (float) getWidth(), (float) getHeight(), this.mPaint);
////        invalidate();
////    }
//
////    public void setBackgroundColor(int color) {
////        this.mBackgroundColor = color;
////        this.mPaint.setColor(this.mBackgroundColor);
////        invalidate();
////    }
//
//
//
//    @Override
//    public void setBackgroundColor(int color) {
//        super.setBackgroundColor(color);
//
//    }
    @Override
    protected void onDraw(Canvas canvas) {
        //这里也调用了

//        mDrawBitmap1 = createBitmap1(width, height);
//        canvas.drawBitmap(mDrawBitmap1, 0, 0, mPaint);
        //canvas绘制到mDrawBitmap   mPaint打印值为 黑色 应该是画笔
        canvas.drawBitmap(mDrawBitmap, 0, 0, mPaint);
//        int bgcolors = spUtils.getInt("bgcolors");
//        Log.i(TAG, "onDraw: " + bgcolors);
//        if(TextUtils.isEmpty(String.valueOf(bgcolors))){
//            mTestPaint.setColor(Color.WHITE);
//        }else{
//            mTestPaint.setColor(bgcolors);
//        }
//        canvas.drawRect(0, 0, getWidth() / 2, getHeight() / 2, mTestPaint);

        //绘制path到canvas
        if (mShape != null && !isClearScreen) {
            mShape.draw(canvas);
        } else if (isClearScreen) {
            isClearScreen = false;
        }
    }

    //创建白色背景的bitmap
    public Bitmap createBitmap(int width, int height) {
        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        Bitmap bitmap = Bitmap.createBitmap(width,
                height, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(bitmap);
        mCanvas.drawRect(0, 0, width, height, paint);
        mCanvas.drawBitmap(bitmap, 0, 0, paint);
        return bitmap;
    }

//    //创建白色背景的bitmap
//    public Bitmap createBitmap1(int width, int height) {
//        Paint paint = new Paint();
////        paint.setColor(Color.WHITE);
//        if(TextUtils.isEmpty(String.valueOf(spUtils.getInt("bgcolors")))){
//            paint.setColor(Color.WHITE);
//        }else{
//            paint.setColor(spUtils.getInt("bgcolors"));
//        }
//        //        drawBackground(canvas);
//        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
//        mCanvas = new Canvas(bitmap);
//        mCanvas.drawRect(0, 0, width, height, paint);
//        mCanvas.drawBitmap(bitmap, 0, 0, paint);
//        return bitmap;
//    }

    private int beijingyanse;

    public void setBeijingyanse(int beijingyanse) {
        this.beijingyanse = beijingyanse;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int currentX = (int) event.getX();
        int currentY = (int) event.getY();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
//                if (mDownAction != null) {
//                    mDownAction.dealDownAction();
//                }
                int mStartX = (int) event.getX();
                int mStartY = (int) event.getY();
                //类型选择
                switch (mDrawType) {
                    case Type.CURVE:
                        // 画笔为 曲线
                        mShape = new CurveShape();
                        break;
                    case Type.WIPE:
                        // 画笔为 橡皮擦
                        mShape = new WipeShape();
                        break;
                    case Type.RECTANGLE:
                        // 画笔为
                        mShape = new RectShape();
                        break;
                    case Type.OVAL:
                        // 画笔为
                        mShape = new OvalShape();
                        break;
                    case Type.LINE:
                        // 画笔为 直线
                        mShape = new LineShape();
                        break;
                    case Type.MULTI_LINE:
                        // 画笔为 方块
                        mShape = new MultiLineShape();
                        break;
                }
                mShape.touchDown(mStartX, mStartY);
                return true;

            case MotionEvent.ACTION_MOVE:
                mShape.touchMove(currentX, currentY);
                invalidate();
                return true;

            case MotionEvent.ACTION_UP:
                mShape.touchUp(currentX, currentY);
                //把之前的path保存绘制到mDrawBitmap上
                ShapeResource resource = new ShapeResource();
                if (mShape instanceof WipeShape) {
                    resource.mType = Type.WIPE;
                    resource.mCurvePath = ((WipeShape) mShape).getPath();
                } else if (mShape instanceof CurveShape) {
                    resource.mType = Type.CURVE;
                    resource.mCurvePath = ((CurveShape) mShape).getPath();
                } else if (mShape instanceof LineShape) {
                    saveShapeResource(resource, Type.LINE);
                } else if (mShape instanceof OvalShape) {
                    saveShapeResource(resource, Type.OVAL);
                } else if (mShape instanceof RectShape) {
                    saveShapeResource(resource, Type.RECTANGLE);
                } else if (mShape instanceof MultiLineShape) {
                    //多边形分解成直线
                    saveShapeResource(resource, Type.LINE);
                }
                mSavePath.add(resource);
                invalidate();
                mShape.draw(mCanvas);
                return true;

            default:
                return false;
        }

    }

    //每次ACTION_UP事件保存路径参数
    public void saveShapeResource(ShapeResource resource, int type) {
        resource.mType = type;
        resource.mStartX = (mShape).getStartX();
        resource.mStartY = (mShape).getStartY();
        resource.mEndX = (mShape).getEndX();
        resource.mEndY = (mShape).getEndY();
        resource.mPaint = mShape.getPaint();
    }

    //撤回
    public void reCall() {
        if (mSavePath.size() == 0) {
            Toast.makeText(getContext(), "对不起，不能撤回", Toast.LENGTH_SHORT).show();
            return;
        }
        ShapeResource resource = mSavePath.get(mSavePath.size() - 1);
        mDeletePath.add(resource);
        mSavePath.remove(mSavePath.size() - 1);
        updateBitmap();
        isRecentRecallOrUndo = true;
    }

    //恢复
    public void undo() {
        if (mDeletePath.size() == 0) {
            Toast.makeText(getContext(), "对不起，不能恢复", Toast.LENGTH_SHORT).show();
            return;
        }
        ShapeResource resource = mDeletePath.get(mDeletePath.size() - 1);
        if (mShape instanceof MultiLineShape) {
            if (resource.mStartX != MultiLineShape.getNextPointX() || resource.mStartY != MultiLineShape.getNextPointY()) {
                Toast.makeText(getContext(), "对不起，不能恢复", Toast.LENGTH_SHORT);
                return;
            } else {
                MultiLineShape.setNewStartPoint(resource.mEndX, resource.mEndY);
            }
        }
        mSavePath.add(resource);
        mDeletePath.remove(mDeletePath.size() - 1);
        updateBitmap();
        isRecentRecallOrUndo = true;
    }

    //更新bitmap
    public void updateBitmap() {
        mDrawBitmap.eraseColor(Color.WHITE);
        mCanvas = new Canvas(mDrawBitmap);
        isClearScreen = true;
        invalidate();
        for (ShapeResource resource : mSavePath) {
            switch (resource.mType) {
                case Type.WIPE:
                case Type.CURVE:
                    mCanvas.drawPath(resource.mCurvePath, resource.mCurvePath.mPaint);
                    break;
                case Type.LINE:
                    mCanvas.drawLine(resource.mStartX, resource.mStartY,
                            resource.mEndX, resource.mEndY, resource.mPaint);
                    break;
                case Type.OVAL:
                    RectF rectF = new RectF(resource.mStartX, resource.mStartY,
                            resource.mEndX, resource.mEndY);
                    mCanvas.drawOval(rectF, resource.mPaint);
                    break;

                case Type.RECTANGLE:
                    mCanvas.drawRect(resource.mStartX, resource.mStartY,
                            resource.mEndX, resource.mEndY, resource.mPaint);
                    break;
            }
        }
    }

    //清屏
    public void clearScreen() {
        mDrawBitmap.eraseColor(Color.WHITE);
        mCanvas = new Canvas(mDrawBitmap);
        mSavePath.clear();
        mDeletePath.clear();
        isClearScreen = true;
        mShape = null;
        MultiLineShape.clear();
        invalidate();
    }



    //从本地文件读取的笔记
    public void updateDrawFromPaths(List<ShapeResource> data) {
        clearScreen();
        mSavePath.addAll(data);
        loadBitmapFromLocal();
        Log.e("zjl","setDrawPaths : data.size() : " + data.size());
    }

    //加载本地文件笔记
    public void loadBitmapFromLocal() {
        for (ShapeResource resource : mSavePath) {
            if (resource.mPaint != null) {
                resource.mPaint.loadPaint();
            }
            switch (resource.mType) {
                case Type.WIPE:
                case Type.CURVE:
                    resource.mCurvePath.loadPathPointsAsQuadTo();
                    resource.mCurvePath.mPaint.loadPaint();
                    mCanvas.drawPath(resource.mCurvePath, resource.mCurvePath.mPaint);
                    break;

                case Type.LINE:
                    mCanvas.drawLine(resource.mStartX, resource.mStartY,
                            resource.mEndX, resource.mEndY, resource.mPaint);
                    break;

                case Type.OVAL:
                    RectF rectF = new RectF(resource.mStartX, resource.mStartY,
                            resource.mEndX, resource.mEndY);
                    mCanvas.drawOval(rectF, resource.mPaint);
                    break;

                case Type.RECTANGLE:
                    mCanvas.drawRect(resource.mStartX, resource.mStartY,
                            resource.mEndX, resource.mEndY, resource.mPaint);
                    break;
            }
        }
    }

    //设置绘图类型
    public void setDrawType(int type){
        //如果点击撤回或者恢复后重新绘制，则清空mDeletePath
        if(isRecentRecallOrUndo){
            mDeletePath.clear();
            isRecentRecallOrUndo = false;
        }
        mDrawType = type;
    }

    public Bitmap getDrawBitmap(){

//            Bitmap whiteBgBitmap = Bitmap.createBitmap(mDrawBitmap.getWidth(), mDrawBitmap.getHeight(),
//                    Bitmap.Config.ARGB_8888);
//            Canvas canvas = new Canvas(whiteBgBitmap);
//            canvas.drawColor(Color.WHITE);
//            canvas.drawBitmap(mDrawBitmap, 0, 0, mPaint);
            return mDrawBitmap;
    }

    //获取笔记路径
    public ArrayList<ShapeResource> getNotePath(){
        return mSavePath;
    }

    //无用方法
    public interface OnDownAction {
        void dealDownAction();
    }
}
