package com.example.mysketchpad.white;

import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;

import java.io.Serializable;

/**
 * Created by zhou on 2020/2/12 21:20.
 */
public class WritablePaint extends Paint implements Serializable {

    public int mColor;

    public float mWidth;

    public float mEraserWidth;

    public boolean isEraser = false;

    public void loadPaint(){
        setAntiAlias(true);
        setColor(mColor);
        setStyle(Style.STROKE);
        setStrokeWidth(mWidth);
        setDither(true);
        setStrokeJoin(Paint.Join.ROUND);
        setStrokeCap(Paint.Cap.ROUND);
        if(isEraser){
            setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
            setAlpha(0);
            setStrokeWidth(mWidth);
        }
    }

}
