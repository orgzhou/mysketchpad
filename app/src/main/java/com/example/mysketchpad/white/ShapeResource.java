package com.example.mysketchpad.white;

import java.io.Serializable;

/**
 * Created by zhou on 2020/2/13 16:10.
 */
//形状资源
public class ShapeResource implements Serializable {

    public int mType;
    public float mStartX;
    public float mStartY;
    public float mEndX;
    public float mEndY;

    //可写画笔
    public WritablePaint mPaint;

    //针对曲线：包含了一个WritablePaint
    //曲线路径
    public WritablePath mCurvePath;

}
