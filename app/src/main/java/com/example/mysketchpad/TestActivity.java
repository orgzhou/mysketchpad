package com.example.mysketchpad;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.hjq.permissions.OnPermission;
import com.hjq.permissions.XXPermissions;
import com.kongzue.titlebar.TitleBar;
import com.kongzue.titlebar.interfaces.OnRightButtonPressed;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class TestActivity extends AppCompatActivity {
    TitleBar titleBar;
    ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        requestPermission();
        imageView = findViewById(R.id.hahahaa);
        titleBar = findViewById(R.id.titleBar);
        Bitmap bitmap = ((BitmapDrawable)imageView.getDrawable()).getBitmap();
        titleBar.setOnRightButtonPressed(new OnRightButtonPressed() {
            @Override
            public void onRightButtonPressed(View v) {
                Toast.makeText(TestActivity.this,"点击了右侧按钮",Toast.LENGTH_SHORT).show();
                saveImageToGallery(TestActivity.this,bitmap);
            }
        });




    }

    public void requestPermission(){
        XXPermissions.with(this)
                .permission(Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .request(new OnPermission() {
                    @Override
                    public void hasPermission(List<String> granted, boolean isAll) {
                        if (isAll) {
                            Toast.makeText(TestActivity.this,
                                    "获取权限成功",Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(TestActivity.this,
                                    "获取权限成功，部分权限未正常授予",Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void noPermission(List<String> denied, boolean quick) {
                        if(quick) {
                            Log.e("zjl",quick+""+denied);
                            //如果是被永久拒绝就跳转到应用权限系统设置页面
                            Toast.makeText(TestActivity.this,
                                    "被永久拒绝授权，请手动授予权限",Toast.LENGTH_SHORT).show();
                            XXPermissions.gotoPermissionSettings(TestActivity.this);
                        }else {
                            Toast.makeText(TestActivity.this,
                                    "获取权限失败",Toast.LENGTH_SHORT).show();
                        }
                    }

                });
    }

    //保存资源文件中的图片到本地相册,实时刷新
    public static void saveImageToGallery(Context context, Bitmap bmp) {
        // 首先保存图片
        File appDir = new File(Environment.getExternalStorageDirectory(), "ZJLHAHA");
        Log.e("zjl",appDir+"");
        if (!appDir.exists()) {
            appDir.mkdir();
        }
        String fileName = "haha"+System.currentTimeMillis() + ".jpg";
        File file = new File(appDir, fileName);
        try {
            FileOutputStream fos = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 其次把文件插入到系统图库
        try {
            MediaStore.Images.Media.insertImage(context.getContentResolver(),
                    file.getAbsolutePath(), fileName, null);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        // 最后通知图库更新
        context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse(file.getAbsolutePath())));
        Log.e("zjl",""+file.getAbsolutePath());
    }

}
