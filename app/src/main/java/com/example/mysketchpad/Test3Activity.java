package com.example.mysketchpad;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.mysketchpad.white.ColorAdapter;
import com.example.mysketchpad.white.ColorBean;
import com.example.mysketchpad.white.ColorJlAdapter;
import com.kongzue.dialog.v3.CustomDialog;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Test3Activity extends AppCompatActivity  {

    @BindView(R.id.btn_tanqi)
    Button btnTan;
    @BindView(R.id.txt_yansess)
    TextView textView;
//    private List<ColorBean> colorBeanList = new ArrayList<>();
    //color selector
    SPUtils spUtils = SPUtils.getInstance();
    List<Integer> integerList = new ArrayList<>();
    Set<Integer> integerSet = new HashSet<>();
    private ColorJlAdapter colorzAdapter;
    public static String COLORS2 = "";
    //这里就是 用了个数组装
    public static final Integer[] COLORS = {
            new Integer(Color.BLACK), new Integer(0xFFFFC125),new Integer(0xFFFF82AB),new Integer(0xFFFFD39B),
            new Integer(0xFFFFBBFF),new Integer(0xFF7FFFD4),new Integer(0xFF7D26CD),new Integer(0xFFC0FF3E),
            new Integer(0xFFFF8247),new Integer(0xFFFF34B3),new Integer(0xFFFF0000),new Integer(0xFFFF00FF),
            new Integer(0xFFFF6A6A),new Integer(0xFFFF4500),new Integer(0xFFC1CDCD),new Integer(0xFFC6E2FF),
            new Integer(0xFF32CD32),new Integer(0xFF228B22),new Integer(0xFF191970),new Integer(0xFF9B30FF),
            new Integer(0xFF8B0A50),new Integer(0xFF8B3A3A),new Integer(0xFF8B7500),new Integer(0xFFC6E2FF),
            new Integer(0xFF218868),new Integer(0xFF2F4F4F),new Integer(0xFFCD5B45),new Integer(0xFFEE7600)
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test3);
        ButterKnife.bind(this);

    }

    @OnClick({R.id.btn_tanqi})
    void onClick(){
        showDialog();
    }

    public void showDialog(){
        CustomDialog.show(Test3Activity.this, R.layout.layout_testz_dialog, new CustomDialog.OnBindView() {
            @Override
            public void onBind(CustomDialog dialog, View v) {
                RecyclerView rvJilu = v.findViewById(R.id.rv_zuijin);
                RecyclerView rvMoren = v.findViewById(R.id.rv_morenyanse);
                integerList.clear();
                //横向 线性
                if(spUtils.getString("colorList").equals("")){
                    Log.e("TAG","ZZZZ");

                }else{
                    Log.e("TAG","分割前"+spUtils.getString("colorList"));
                    String[] strList=spUtils.getString("colorList").split(",");
                    //循环取出strList里的颜色
                    for (int t = 0; t < strList.length; t++) {
                        integerList.add(Integer.valueOf(strList[t]));
                    }

                }
//                Log.e("TAG",integerList.size()+" /n "+spUtils.getInt("colorValue"));
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(Test3Activity.this);
                linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                //这个rv就是 记录 颜色 的 列表
                rvJilu.setLayoutManager(linearLayoutManager);
                colorzAdapter = new ColorJlAdapter(Test3Activity.this,integerList);
                rvJilu.setAdapter(colorzAdapter);

                //
                rvMoren.setLayoutManager(new GridLayoutManager(Test3Activity.this,6));
                List<Integer> listColor = Arrays.asList(COLORS);
                ColorAdapter adapter = new ColorAdapter(Test3Activity.this,R.layout.item_color,listColor);
                rvMoren.setAdapter(adapter);
                adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                       Log.e("zjl",adapter.getData().get(position)+"");
                       int color = (int) adapter.getData().get(position);
                       Log.e("TAG",color+"");

                        Log.e("TAG","if判断前"+(spUtils.getString("colorValue")));

                        if(!TextUtils.isEmpty(spUtils.getString("colorValue"))){

                            if("".equals(spUtils.getString("colorList"))){

                                Log.e("TAG","2层if里面"+spUtils.getString("colorValue")+",");
                                COLORS2=spUtils.getString("colorValue")+",";
                                Log.e("TAG","2层if里面1"+COLORS2);

                            }else{
                                String[] colorSplit = spUtils.getString("colorList").split(",");
                                List<String> color2List = Arrays.asList(colorSplit);

                                if(color2List.contains(String.valueOf(color))){
//                                    Toast.makeText(Test3Activity.this,"已经选中过该颜色",Toast.LENGTH_SHORT).show();
                                    ToastUtils.showShort("已经选中过该颜色");
                                    COLORS2=spUtils.getString("colorList");
                                    Log.e("TAG","2层if里面2"+COLORS2);
                                }else{
                                    COLORS2=spUtils.getString("colorList")+color+",";
                                    Log.e("TAG","2层if里面3"+COLORS2);
                                }

                            }
                        }else{
                            COLORS2=color+",";
                        }

                        spUtils.put("colorValue",String.valueOf(color));
                        spUtils.put("colorList",COLORS2);
                        Log.e("TAG","ZZZZZZ"+COLORS2);

                        dialog.doDismiss();
                    }
                });
            }
        }).setCancelable(false);
    }


}
