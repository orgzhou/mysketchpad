package com.example.mysketchpad;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.gyf.immersionbar.ImmersionBar;
import com.hjq.permissions.OnPermission;
import com.hjq.permissions.XXPermissions;

import java.util.List;

//项目二
//看明白了吗 我操作一遍
//项目一   我进行提交
//提交2
//提交333
public class MainActivity extends AppCompatActivity {

    @BindView(R.id.main_setting)
    ImageView main_setting_img;
    @BindView(R.id.main_huaban)
    LinearLayout main_huaban_ll;
    @BindView(R.id.main_tuya)
    LinearLayout main_tuya_ll;
    @BindView(R.id.main_wode)
    LinearLayout main_wode_ll;
    @BindView(R.id.main_mh)
    LinearLayout main_mh_ll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        ImmersionBar.with(this).transparentBar().statusBarDarkFont(true).init();
        requestPermission();
    }

    @OnClick({R.id.main_huaban,R.id.main_tuya,R.id.main_wode,R.id.main_mh,R.id.main_setting})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.main_huaban:
                Toast.makeText(this,"您点击了画板",Toast.LENGTH_SHORT).show();
                Log.e("zjl","您点击了画板");
                startActivity(new Intent(MainActivity.this,HuaBanActivity.class));
                break;

            case R.id.main_tuya:
                Toast.makeText(this,"您点击了涂鸦",Toast.LENGTH_SHORT).show();
                break;

            case R.id.main_wode:
                Toast.makeText(this,"您点击了我的",Toast.LENGTH_SHORT).show();
                startActivity(new Intent(MainActivity.this,MineActivity.class));
                break;

            case R.id.main_mh:
                Toast.makeText(this,"您点击了名画赏析",Toast.LENGTH_SHORT).show();
                break;

            case R.id.main_setting:
                Toast.makeText(this,"您点击了设置",Toast.LENGTH_SHORT).show();
                break;

        }
    }

    //权限请求
    public void requestPermission(){
        XXPermissions.with(this)
                .permission(Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .request(new OnPermission() {
                    @Override
                    public void hasPermission(List<String> granted, boolean isAll) {
                        if (isAll) {
                            Toast.makeText(MainActivity.this,
                                    "获取权限成功",Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(MainActivity.this,
                                    "获取权限成功，部分权限未正常授予",Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void noPermission(List<String> denied, boolean quick) {
                        if(quick) {
                            Log.e("zjl",quick+""+denied);
                            //如果是被永久拒绝就跳转到应用权限系统设置页面
                            Toast.makeText(MainActivity.this,
                                    "被永久拒绝授权，请手动授予权限",Toast.LENGTH_SHORT).show();
                            XXPermissions.gotoPermissionSettings(MainActivity.this);
                        }else {
                            Toast.makeText(MainActivity.this,
                                    "获取权限失败",Toast.LENGTH_SHORT).show();
                        }
                    }

                });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}
