package com.example.mysketchpad.util;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.blankj.utilcode.util.SPUtils;
import com.example.mysketchpad.white.bean.LocalBitmap;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhou on 2020/3/8 19:51.
 */
public class MySharedpreferencedUtils {

    public static void saveToLocal(Context context, LocalBitmap bitmap) {
        Log.i("zjl::", "saveToLocal: " + bitmap.toString());
        //获取传入的是否为null
        if (bitmap == null) return;
        //不为null,获取一遍
        List<LocalBitmap> localBitmapList = getLocalData();
        if (localBitmapList == null) {
            localBitmapList = new ArrayList<>();
        }
        //gson实例化
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        //将实体类装入List
        localBitmapList.add(bitmap);
        Type type=new TypeToken<List<LocalBitmap>>(){}.getType();
        String value = gson.toJson(localBitmapList,type);
        SPUtils.getInstance().put("GsonLocalBitmap", value);
        Log.v("zjl::","存入时候的SP::" + value);
        Toast.makeText(context, "保存成功", Toast.LENGTH_SHORT).show();
    }

    public static List<LocalBitmap> getLocalData() {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        String mineModelGson = SPUtils.getInstance().getString("GsonLocalBitmap");
        Log.i("zjl::", "onCreate: " + mineModelGson);
        Type type=new TypeToken<List<LocalBitmap>>(){}.getType();
        //list是解析出来的数据
        List<LocalBitmap> localBitmapList = gson.fromJson(mineModelGson,type);
        return localBitmapList;
    }
}
